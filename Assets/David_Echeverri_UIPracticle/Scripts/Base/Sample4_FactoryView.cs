﻿using UnityEngine;

public class Sample4_FactoryView
{
    //Crea modulos segun el Nombre.
    public static UIBase CreateView(UIView view, Transform parent)
    {
        string path = "View/" + view.ToString();
        GameObject newView = GameObject.Instantiate(Resources.Load(path)) as GameObject;
        if (newView != null)
        {
            newView.transform.SetParent(parent.transform);
            newView.transform.localPosition = Vector3.zero;
            newView.transform.localScale = Vector3.one;
        }
        return newView.GetComponent<UIBase>();
    }

    
}
